import AppNavbar from './components/AppNavbar';
/*import Home from './pages/Home';
import Courses from './pages/Courses';*/
import Register from './pages/Register';

import './App.css';


import { Container} from 'react-bootstrap';

function App() {
  return (
    <>
    <AppNavbar />
    <Container>
      <Register />
    </Container >
    </>
  );
}

export default App;



